;(function() {
  "use strict";

  let nextKey = $('.next-step'),
    prevKey = $('.prev-step'),
    resetKey = $('.reset'),
    elemForm = $('form');


  elemForm.validate();

  nextKey.on('click', function() {
    form.nextStep();
  });

  prevKey.on('click', function() {
    form.prevStep();
  });

  resetKey.on('click', function() {
    form.reset();
  });

  let form = new Form();

  function Form() {
    let report = {},
        currentStep = 1,
        stepCount = elemForm.length;

    function nextStep() {
      if (currentStep < stepCount) {
        let _isValid = $(`#form${currentStep}`).valid();

        if (!_isValid)
          return false;

        setData.call(this, currentStep);
        currentStep++;
        showCurrentForm.call(this);
        renderStepNumb(currentStep);
      }

      if (currentStep === stepCount) {
        getReport.call(this, currentStep);
      }
    }

    function prevStep() {
      if (currentStep > 1) {
        currentStep--;
        showCurrentForm.call(this);
        renderStepNumb(currentStep);
      }
    }

    function reset() {
      elemForm.each(function(i) {
        let id = $(this).attr('id');
        $(`#${id}`).get(0).reset();
      });
    }

    function setData(formId) {
      let elem = $('.field'),
        fields = $(`[data-step=${formId}]`).find(elem);

      fields.each(function(i) {
        let val = $(this).val(),
          name = $(this).attr('name'),
          type = $(this).attr('type'),
          caption = $(this).attr('data-row-title'),
          noCheckVal = "No checked";

        if (type === 'radio' || type === 'checkbox')
          val = $(`input[name=${name}]:checked`).val();

        if (type === 'checkbox' && val === undefined)
          val = noCheckVal;

          report['' + caption + ''] = val;

      });
    }

    function renderStepNumb(stepId) {
      let step = $('.step-id'),
        progress = $('.progress-bar'),
        progressStep = 100 / stepCount;

      step.text(stepId);
      progress.css('width', `${progressStep * stepId}%`);
    }

    function getReport(stepId) {
      let reportBox = $('.report');

      if (stepId = stepCount) {
        for (var property in report) {
          reportBox.append(
            `<div class="item"><span class="item-title"> ${property}
             </span> <span class="item-value"> ${report[property]} </span><div>`
           );
        }
      }
    }

    function showCurrentForm() {
      elemForm.removeClass('show');
      $(`[data-step=${currentStep}]`).addClass('show');
    }

    return{
      nextStep,
      prevStep,
      reset
    }
  }
})();
